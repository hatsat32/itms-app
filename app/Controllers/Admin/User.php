<?php namespace App\Controllers\Admin;

use App\Core\AdminController;
use App\Models\User as UserModel;
use Myth\Auth\Models\UserModel as AuthUserModel;
use Myth\Auth\Entities\User as AuthUser;

class User extends AdminController
{
	protected $userModel = null;

	public function initController($request, $response, $logger)
	{
		parent::initController($request, $response, $logger);

		$this->userModel = new UserModel();
	}
	function index()
	{
		$users = $this->userModel->findAll();
		return view('admin/user/index', ['users' => $users]);
	}

	function new()
	{
		return view('admin/user/new');
	}

	function show(int $id = null)
	{
		$user = $this->userModel->find($id);
		return view('admin/user/detail', ['user' => $user]);
	}

	function create()
	{
		$authUserModel = new AuthUserModel();

		$data = [
			'username'      => $this->request->getPost('username'),
			'email'         => $this->request->getPost('email'),
			'active'        => $this->request->getPost('active'),
			'password'      => $this->request->getPost('password'),
		];

		$rules = array_merge($authUserModel->getValidationRules(['only' => ['username']]), [
			'email'      => 'required|valid_email|is_unique[users.email]',
			'password'   => 'required|min_length[3]',
		]);

		if (! $this->validate($rules))
		{
			return redirect()->back()->withInput()->with('errors', $authUserModel->errors());
		}

		$user = new AuthUser();
		$user->fill($data);
		$result = $authUserModel->insert($user);

		if (! $result)
		{
			return redirect()->back()->withInput()
					->with('errors', $authUserModel->errors());
		}

		return redirect()->route('adm-users-show', [$result]);
	}

	function delete(int $id = null)
	{
		$this->userModel->delete($id);
		return redirect('adm-users');
	}

	function update(int $id = null)
	{
		$authUserModel = new AuthUserModel();
		$user = $authUserModel->find($id);
		$user->fill($this->request->getPost());

		$result = $authUserModel->save($user);

		if (! $result)
		{
			return redirect()->back()->with('errors', $authUserModel->errors());
		}

		return redirect()->back()->with('message', 'User Updated');
	}

	public function change_password(int $id = null)
	{
		$authUserModel = new AuthUserModel();
		$user = $authUserModel->find($id);
		$user->fill($this->request->getPost());

		$rules = [
			'password'     => 'required|strong_password',
			'pass_confirm' => 'required|matches[password]',
		];

		if (! $this->validate($rules))
		{
			return redirect()->back()->with('errors', $this->validator->getErrors());
		}

		$user->setPassword($this->request->getPost('password'));
		$authUserModel->save($user);

		return redirect()->back()->with('message', 'Password Changed');
	}
}
