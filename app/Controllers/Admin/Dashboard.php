<?php namespace App\Controllers\Admin;

use App\Core\AdminController;

class Dashboard extends AdminController
{
	function index()
	{
		return view('admin/dashboard');
	}
}

