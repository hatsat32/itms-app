<?php namespace App\Controllers\Admin;

use App\Core\AdminController;
use App\Models\Message as MessageModel;

class Message extends AdminController
{
	protected $messageModel = null;

	public function initController($request, $response, $logger)
	{
		parent::initController($request, $response, $logger);

		$this->messageModel = new MessageModel();
	}

	function index()
	{
		$messages = $this->messageModel->findAll();
		return view('admin/message/index', ['messages' => $messages]);
	}

	function show(int $id = null)
	{
		$message = $this->messageModel->find($id);
		return view('admin/message/detail', ['message' => $message]);
	}

	function delete(int $id = null)
	{
		$result = $this->messageModel->delete($id);

		if (! $result)
		{
			return redirect()->back()->with('errors', $this->messageModel->errors());
		}

		return redirect()->route('adm-messages');
	}

	function update(int $id = null)
	{
		$post = $this->messageModel->find($id);
		$post->fill($this->request->getPost());

		$result = $this->messageModel->save($post);

		if (! $result)
		{
			return redirect()->back()->with('errors', $this->messageModel->errors());
		}

		return redirect()->back();
	}
}
