<?php namespace App\Controllers\Admin;

use App\Core\AdminController;
use App\Models\Module as ModuleModel;

class Module extends AdminController
{
	protected $moduleModel = null;


	public function initController($request, $response, $logger)
	{
		parent::initController($request, $response, $logger);

		$this->moduleModel = new ModuleModel();
	}

	function index()
	{
		$modules = $this->moduleModel->findAll();
		return view('admin/modules/index', ['modules' => $modules]);
	}

	function new()
	{
		return view('admin/modules/new');
	}

	function show(int $id = null)
	{
		$module = $this->moduleModel->find($id);

		return view('admin/modules/detail', ['module' => $module]);
	}

	function create()
	{
		$data = [
			'name'        => $this->request->getPost('name'),
			'description' => $this->request->getPost('description'),
		];

		$result = $this->moduleModel->insert($data);

		if (! $result)
		{
			return redirect()->back()->withInput()
					->with('errors', $this->moduleModel->errors());
		}

		return redirect()->route('adm-modules-show', [$result]);
	}

	function delete(int $id = null)
	{
		$result = $this->moduleModel->delete($id);

		if (! $result)
		{
			return redirect()->back()->with('errors', $this->moduleModel->errors());
		}

		return redirect()->route('adm-modules');
	}

	function update(int $id = null)
	{
		$module = $this->moduleModel->find($id);
		$module->fill($this->request->getPost());

		$result = $this->moduleModel->save($module);

		if (! $result)
		{
			return redirect()->back()->with('errors', $this->moduleModel->errors());
		}

		return redirect()->back();
	}
}
