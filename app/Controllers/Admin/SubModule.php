<?php namespace App\Controllers\Admin;

use App\Core\AdminController;
use App\Models\SubModule as SubModuleModel;
use App\Models\Module as ModuleModel;

class SubModule extends AdminController
{
	protected $subModuleModel = null;

	public function initController($request, $response, $logger)
	{
		parent::initController($request, $response, $logger);

		$this->subModuleModel = new SubModuleModel();
	}

	function index()
	{
		$submodules = $this->subModuleModel
				->select(['sub_modules.*', 'modules.name AS module_name'])
				->join('modules', 'sub_modules.module_id = modules.id')
				->findAll();
		return view('admin/submodules/index', ['submodules' => $submodules]);
	}

	function new()
	{
		$modules = (new ModuleModel())->findAll();
		return view('admin/submodules/new', ['modules' => $modules]);
	}

	function show(int $id = null)
	{
		$submodule = $this->subModuleModel->find($id);
		$modules = (new ModuleModel())->findAll();

		return view('admin/submodules/detail', [
			'modules'   => $modules,
			'submodule' => $submodule,
		]);
	}

	function create()
	{
		$data = [
			'module_id'          => $this->request->getPost('module_id'),
			'name'               => $this->request->getPost('name'),
			'purpose'            => $this->request->getPost('purpose'),
			'description'        => $this->request->getPost('description'),
			'svc_activity_contr' => $this->request->getPost('svc_activity_contr'),
		];

		$result = $this->subModuleModel->insert($data);

		if (! $result)
		{
			return redirect()->back()->withInput()
					->with('errors', $this->subModuleModel->errors());
		}

		return redirect()->route('adm-submodules-show', [$result]);
	}

	function delete(int $id = null)
	{
		$result = $this->subModuleModel->delete($id);

		if (! $result)
		{
			return redirect()->back()->with('errors', $this->subModuleModel->errors());
		}

		return redirect()->route('adm-submodules');
	}

	function update(int $id = null)
	{
		$submodule = $this->subModuleModel->find($id);
		$submodule->fill($this->request->getPost());

		$result = $this->subModuleModel->save($submodule);

		if (! $result)
		{
			return redirect()->back()->with('errors', $this->subModuleModel->errors());
		}

		return redirect()->back();
	}
}
