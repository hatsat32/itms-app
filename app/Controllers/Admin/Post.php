<?php namespace App\Controllers\Admin;

use App\Core\AdminController;
use App\Models\Post as PostModel;

class Post extends AdminController
{
	protected $postModel = null;

	public function initController($request, $response, $logger)
	{
		parent::initController($request, $response, $logger);

		$this->postModel = new PostModel();
	}

	function index()
	{
		$posts = $this->postModel->findAll();
		return view('admin/post/index', ['posts' => $posts]);
	}

	function new()
	{
		return view('admin/post/new');
	}

	function show(int $id = null)
	{
		$post = $this->postModel->find($id);

		return view('admin/post/detail', ['post' => $post]);
	}

	function create()
	{
		$data = [
			'type'    => $this->request->getPost('type'),
			'name'    => $this->request->getPost('name'),
			'content' => $this->request->getPost('content'),
		];

		$result = $this->postModel->insert($data);

		if (! $result)
		{
			return redirect()->back()->withInput()
					->with('errors', $this->postModel->errors());
		}

		return redirect()->route('adm-posts-show', [$result]);
	}

	function delete(int $id = null)
	{
		$result = $this->postModel->delete($id);

		if (! $result)
		{
			return redirect()->back()->with('errors', $this->postModel->errors());
		}

		return redirect()->route('adm-posts');
	}

	function update(int $id = null)
	{
		$post = $this->postModel->find($id);
		$post->fill($this->request->getPost());

		$result = $this->postModel->save($post);

		if (! $result)
		{
			return redirect()->back()->with('errors', $this->postModel->errors());
		}

		return redirect()->back();
	}
}
