<?php namespace App\Controllers;

use App\Models\Post;
use App\Models\Module;
use App\Models\SubModule;

class Home extends BaseController
{
	protected $postModel = null;

	//--------------------------------------------------------------------

	public function initController($request, $response, $logger)
	{
		parent::initController($request, $response, $logger);

		$this->postModel = new Post();
	}

	//--------------------------------------------------------------------

	public function index()
	{
		$subModuleModel = new SubModule();
		$subdomules = $subModuleModel
				->select(['sub_modules.*', 'modules.name AS module_name'])
				->join('modules', 'modules.id = sub_modules.module_id', 'left')
				->findAll();

		$services = array_filter($subdomules, function($s) {
			return stripos($s->module_name, 'Service') !== false;
		});
		
		$generals = array_filter($subdomules, function($s) {
			return stripos($s->module_name, 'General') !== false;
		});
		
		$technicals = array_filter($subdomules, function($s) {
			return stripos($s->module_name, 'Technical') !== false;
		});

		return view('frontend/index', [
			'services'   => $services,
			'generals'   => $generals,
			'technicals' => $technicals,
		]);
		// return view('home');
	}

	//--------------------------------------------------------------------

	public function cobit()
	{
		$cobit_post = $this->postModel->where('type', 'cobit')->first();
		return view('cobit', ['cobit_post' => $cobit_post]);
	}

	//--------------------------------------------------------------------

	public function itil($sub = null)
	{
		$modules = (new Module())->findAll();
		$submodules = (new SubModule())->findAll();

		foreach ($modules as $i => $module)
		{
			$modules[$i]->submodules = array_filter($submodules, function($submodule) use ($module) {
				return $submodule->module_id == $module->id;
			});
		}

		$submodule_detail = ( $sub == null ? null:(new SubModule())->find($sub) );

		return view('frontend/itil', [
			'modules' => $modules,
			'submodule_detail' => $submodule_detail,
		]);
	}

	//--------------------------------------------------------------------

	public function iso27001()
	{
		$iso27001_post = $this->postModel->where('type', 'iso27001')->first();
		return view('iso27001', ['iso27001_post' => $iso27001_post]);
	}

	//--------------------------------------------------------------------

	public function iso9001()
	{
		$iso9001_post = $this->postModel->where('type', 'iso9001')->first();
		return view('iso9001', ['iso9001_post' => $iso9001_post]);
	}

	//--------------------------------------------------------------------

	public function relations()
	{
		$postModel = new Post();
		$posts = $postModel->findAll();
		return view('frontend/relations', ['posts' => $posts]);
		// return view('relations', ['posts' => $posts]);
	}

	//--------------------------------------------------------------------

	public function relation($id)
	{
		$post = (new Post())->find($id);
		return view('frontend/relation', ['post' => $post]);
		// return view('post_detail', ['post' => $post]);
	}

	//--------------------------------------------------------------------

	public function about()
	{
		return view('frontend/about');
		// return view('home');
	}

	//--------------------------------------------------------------------

	public function message()
	{
		$messageModel = new \App\Models\Message();

		$data = [
			'name'    => $this->request->getPost('name'),
			'email'   => $this->request->getPost('email'),
			'message' => $this->request->getPost('message'),
		];

		if (! $messageModel->insert($data) )
		{
			return redirect()->to('/about')->with('errors', $messageModel->errors())->withInput();
		}
		
		return redirect()->to('/about')->with('message', 'Your message saved');
	}

	//--------------------------------------------------------------------
}
