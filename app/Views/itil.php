<?= $this->extend("templates/base_sidebar") ?>

<?= $this->section('content') ?>

	<div class="my-4">

		<?php if ($submodule_detail != null) : ?>
			<h1 class="text-center display-4"><?= $submodule_detail->name ?></h1>

			<h2>Purpose</h2>
			<?= $submodule_detail->purpose ?>

			<hr>

			<h2>Description</h2>
			<?= $submodule_detail->description ?>

			<hr>

			<h2>SVC Activity's Contribution</h2>
			<?= $submodule_detail->svc_activity_contr ?>
		<?php endif ?>

	</div>

<?= $this->endSection() ?>