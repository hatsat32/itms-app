<?= $this->extend("templates/body") ?>

<?= $this->section('body') ?>

	<?= $this->include('templates/navbar') ?>

	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<?= $this->include('templates/sidebar') ?>
			</div>

			<div class="col-lg-8">
				<?= $this->renderSection("content") ?>
			</div>
		</div>
	</div>

<?= $this->endSection() ?>
