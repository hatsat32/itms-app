<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">

		<title>ITMS ARCHIVE</title>

		<link href="/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="/css/blog.css" rel="stylesheet">
	</head>

	<body>
		<?= $this->renderSection("body") ?>

		<?php //echo $this->include('templates/footer') ?>

		<script src="/lib/jquery/jquery-3.4.1.min.js"></script>
		<script src="/lib/popper/popper.min.js"></script>
		<script src="/lib/bootstrap/js/bootstrap.min.js"></script>

		<script>
			$("table").addClass("table");
		</script>
	</body>

</html>
