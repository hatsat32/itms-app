<div class="card my-4">
	<h5 class="card-header">ITIL MODULES</h5>
	<div class="card-body">
		<div class="row">
			<div>
				<ul>
					<?php foreach ($modules as $module) : ?>
						<li>
							<a class="text-primary"><?= $module->name ?></a>
							<?php foreach ($module->submodules as $sub) : ?>
								<ul>
									<li><a href="/itil/<?= esc($sub->id) ?>" class="text-success"><?= $sub->name ?></a></li>
								</ul>
							<?php endforeach ?>
						</li>
					<?php endforeach ?>
				</ul>
			</div>
		</div>
	</div>
</div>
