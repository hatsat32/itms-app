<?= $this->extend("templates/body") ?>

<?= $this->section('body') ?>

	<?= $this->include('templates/navbar') ?>

	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<?= $this->renderSection("content") ?>
			</div>
		</div>
	</div>

<?= $this->endSection() ?>
