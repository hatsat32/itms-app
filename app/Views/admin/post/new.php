<?= $this->extend("admin/templates/base") ?>


<?= $this->section('title') ?>
	New Cobit
<?= $this->endSection() ?>


<?= $this->section('content') ?>

	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="/admin">Dashboard</a>
		</li>
		<li class="breadcrumb-item active">Dashboard</li>
	</ol>

	<div class="card mb-3">
		<div class="card-header">
			<i class="fas fa-chart-area"></i>
			Add Post</div>
		<div class="card-body">
			<?= $this->include('admin/templates/message_block') ?>

			<form action="<?= route_to('adm-posts') ?>" method="post">
				<?= csrf_field() ?>
				<div class="form-group">
					<label for="type">Post Type</label>
					<select name="type" class="form-control" id="type">
						<option value="cobit">Cobit</option>
						<option value="iso27001">ISO 27001</option>
						<option value="iso9001">ISO 9001</option>
					</select>
				</div>
				<div class="form-group">
					<label for="name">Name</label>
					<input type="name" name="name" class="form-control" id="name" value="<?= old('name') ?>">
				</div>
				<div class="form-group">
					<label for="content">Content</label>
					<textarea class="form-control" name="content" id="content" rows="20"><?= old('content') ?></textarea>
				</div>
				<button type="submit" class="btn btn-primary btn-block">ADD POST</button>
			</form>
		</div>
	</div>

<?= $this->endSection() ?>
