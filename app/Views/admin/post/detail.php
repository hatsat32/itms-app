<?= $this->extend("admin/templates/base") ?>


<?= $this->section('title') ?>
	Edit Post
<?= $this->endSection() ?>


<?= $this->section('content') ?>

	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="/admin">Dashboard</a>
		</li>
		<li class="breadcrumb-item active">Post</li>
	</ol>

	<div class="card mb-3">
		<div class="card-header">
			<i class="fas fa-chart-area"></i>
			Edit Post</div>
		<div class="card-body">
			<?= $this->include('admin/templates/message_block') ?>

			<form action="<?= route_to('adm-posts-show', $post->id) ?>" method="post">
				<?= csrf_field() ?>
				<div class="form-group">
					<label for="type">Post Type</label>
					<select name="type" class="form-control" id="type">
						<option value="cobit" <?= $post->type == 'cobit' ? 'selected':'' ?>>Cobit</option>
						<option value="iso27001" <?= $post->type == 'iso27001' ? 'selected':'' ?>>ISO 27001</option>
						<option value="iso9001" <?= $post->type == 'iso9001' ? 'selected':'' ?>>ISO 9001</option>
					</select>
				</div>
				<div class="form-group">
					<label for="name">Name</label>
					<input type="name" name="name" class="form-control" id="name" value="<?= esc($post->name) ?>">
				</div>
				<div class="form-group">
					<label for="content">Content</label>
					<textarea class="form-control" name="content" id="content" rows="20"><?= esc($post->content) ?></textarea>
				</div>
				<button type="submit" class="btn btn-primary btn-block">Update Post</button>
			</form>
			<hr>
			<form action="<?= route_to('adm-posts-delete', $post->id) ?>" method="post">
				<?= csrf_field() ?>
				<button type="submit" class="btn btn-danger btn-block">Delete Post</button>
			</form>
		</div>
	</div>

<?= $this->endSection() ?>
