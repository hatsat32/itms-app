<?= $this->extend("admin/templates/base") ?>


<?= $this->section('title') ?>
	Edit Post
<?= $this->endSection() ?>


<?= $this->section('content') ?>

	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="/admin">Dashboard</a>
		</li>
		<li class="breadcrumb-item active">Post</li>
	</ol>

	<div class="card mb-3">
		<div class="card-header">
			<i class="fas fa-chart-area"></i>
			Edit Post</div>
		<div class="card-body">
			<?= $this->include('admin/templates/message_block') ?>

			<form action="<?= route_to('adm-modules-show', $module->id) ?>" method="post">
				<?= csrf_field() ?>
				<div class="form-group">
					<label for="name">Name</label>
					<input type="name" name="name" class="form-control" id="name" value="<?= esc($module->name) ?>">
				</div>
				<div class="form-group">
					<label for="description">Description</label>
					<textarea class="form-control" name="description" id="description" rows="5"><?= esc($module->description) ?></textarea>
				</div>
				<button type="submit" class="btn btn-primary btn-block">Update Module</button>
			</form>
			<hr>
			<form action="<?= route_to('adm-modules-delete', $module->id) ?>" method="post">
				<?= csrf_field() ?>
				<button type="submit" class="btn btn-danger btn-block">Delete Module</button>
			</form>
		</div>
	</div>

<?= $this->endSection() ?>
