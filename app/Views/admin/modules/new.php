<?= $this->extend("admin/templates/base") ?>


<?= $this->section('title') ?>
	New Cobit
<?= $this->endSection() ?>


<?= $this->section('content') ?>

	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="/admin">Dashboard</a>
		</li>
		<li class="breadcrumb-item active">Dashboard</li>
	</ol>

	<div class="card mb-3">
		<div class="card-header">
			<i class="fas fa-chart-area"></i>
			Module Add</div>
		<div class="card-body">
			<?= $this->include('admin/templates/message_block') ?>

			<form action="<?= route_to('adm-modules') ?>" method="post">
				<?= csrf_field() ?>
				<div class="form-group">
					<label for="name">Name</label>
					<input type="name" name="name" class="form-control" id="name" value="<?= old('name') ?>">
				</div>
				<div class="form-group">
					<label for="description">Content</label>
					<textarea class="form-control" name="description" id="description" rows="5"><?= old('description') ?></textarea>
				</div>
				<button type="submit" class="btn btn-primary btn-block">ADD MODULE</button>
			</form>
		</div>
	</div>

<?= $this->endSection() ?>
