<?= $this->extend("admin/templates/base") ?>


<?= $this->section('title') ?>
	Edit Message - Dashboard
<?= $this->endSection() ?>


<?= $this->section('content') ?>

	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="/admin">Dashboard</a>
		</li>
		<li class="breadcrumb-item">
			<a href="/admin/messages">Messages</a>
		</li>
		<li class="breadcrumb-item active">Message</li>
	</ol>

	<div class="card mb-3">
		<div class="card-header">
			<i class="fas fa-chart-area"></i>
			Edit Post</div>
		<div class="card-body">
			<?= $this->include('admin/templates/message_block') ?>

			<form action="<?= route_to('adm-messages-show', $message->id) ?>" method="post">
				<?= csrf_field() ?>
				<div class="form-group">
					<label for="name">Name</label>
					<input type="name" name="name" class="form-control" id="name" value="<?= esc($message->name) ?>">
				</div>
				<div class="form-group">
					<label for="email">Email</label>
					<input type="email" name="email" class="form-control" id="email" value="<?= esc($message->email) ?>">
				</div>
				<div class="form-group">
					<label for="message">Message</label>
					<textarea class="form-control" name="message" id="message" rows="10"><?= esc($message->message) ?></textarea>
				</div>
				<button type="submit" class="btn btn-primary btn-block">Update Message</button>
			</form>
			<hr>
			<form action="<?= route_to('adm-messages-delete', $message->id) ?>" method="post">
				<?= csrf_field() ?>
				<button type="submit" class="btn btn-danger btn-block">Delete Message</button>
			</form>
		</div>
	</div>

<?= $this->endSection() ?>
