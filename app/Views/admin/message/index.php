<?= $this->extend("admin/templates/base") ?>


<?= $this->section('title') ?>
	Messages - Dashboard
<?= $this->endSection() ?>


<?= $this->section('content') ?>

	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="/admin">Dashboard</a>
		</li>
		<li class="breadcrumb-item">
			<a href="/admin/messages">Messages</a>
		</li>
		<li class="breadcrumb-item active">List</li>
	</ol>

	<div class="card mb-3">
		<div class="card-header">
			<i class="fas fa-user-friends"></i>
			Messages</div>
		<div class="card-body">
			<?= $this->include('admin/templates/message_block') ?>
			<div class="table-responsive">
				<table class="table table-bordered table-striped" id="teams-table" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>id</th>
							<th>name</th>
							<th>email</th>
							<th>detail</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($messages as $m) : ?>
							<tr>
								<td><?= esc($m->id) ?></td>
								<td><?= esc($m->name) ?></td>
								<td><?= esc($m->email) ?></td>
								<td class="p-1">
									<a class="btn btn-info btn-block" href="<?= route_to('adm-messages-show', $m->id) ?>">
										Detail
									</a>
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

<?= $this->endSection() ?>
