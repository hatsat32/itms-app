<?= $this->extend("admin/templates/base") ?>


<?= $this->section('title') ?>
	Edit Post
<?= $this->endSection() ?>


<?= $this->section('content') ?>

	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="/admin">Dashboard</a>
		</li>
		<li class="breadcrumb-item active">Post</li>
	</ol>

	<div class="card mb-3">
		<div class="card-header">
			<i class="fas fa-chart-area"></i>
			Edit Post</div>
		<div class="card-body">
			<?= $this->include('admin/templates/message_block') ?>

			<form action="<?= route_to('adm-submodules-show', $submodule->id) ?>" method="post">
				<?= csrf_field() ?>
				<div class="form-group">
					<label for="module_id">Post Type</label>
					<select name="module_id" class="form-control" id="module_id">
						<?php foreach ($modules as $m) :  ?>
							<option value="<?= $m->id ?>" <?= $submodule->module_id == $m->id ? 'selected':'' ?>>
								<?= esc($m->name) ?>
							</option>
						<?php endforeach ?>
					</select>
				</div>
				<div class="form-group">
					<label for="name">Name</label>
					<input type="name" name="name" class="form-control" id="name" value="<?= esc($submodule->name) ?>">
				</div>
				<div class="form-group">
					<label for="purpose">Purpose</label>
					<textarea class="form-control" name="purpose" id="purpose" rows="5"
							><?= esc($submodule->purpose) ?></textarea>
				</div>
				<div class="form-group">
					<label for="description">Description</label>
					<textarea class="form-control" name="description" id="description" rows="10"
							><?= esc($submodule->description) ?></textarea>
				</div>
				<div class="form-group">
					<label for="svc_activity_contr">Svc Activity Contr</label>
					<textarea class="form-control" name="svc_activity_contr" id="svc_activity_contr"
							rows="10"><?= esc($submodule->svc_activity_contr) ?></textarea>
				</div>
				<button type="submit" class="btn btn-primary btn-block">Update Module</button>
			</form>
			<hr>
			<form action="<?= route_to('adm-submodules-delete', $submodule->id) ?>" method="post">
				<?= csrf_field() ?>
				<button type="submit" class="btn btn-danger btn-block">Delete Module</button>
			</form>
		</div>
	</div>

<?= $this->endSection() ?>
