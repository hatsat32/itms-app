<?= $this->extend("admin/templates/base") ?>


<?= $this->section('title') ?>
	Dashboard
<?= $this->endSection() ?>


<?= $this->section('content') ?>

	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="/admin">Dashboard</a>
		</li>
		<li class="breadcrumb-item active">Dashboard</li>
	</ol>

	<div class="my-4">
		<a class="btn btn-primary btn-block" href="<?= route_to('adm-users-new') ?>">ADD user</a>
	</div>

	<div class="card mb-3">
		<div class="card-header">
			<i class="fas fa-user-friends"></i>
			Posts</div>
		<div class="card-body">
			<?= $this->include('admin/templates/message_block') ?>
			<div class="table-responsive">
				<table class="table table-bordered table-striped" id="teams-table" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>id</th>
							<th>username</th>
							<th>email</th>
							<th>active</th>
							<th>detail</th>
						</tr>
					</thead>
					<tbody>
					<?php foreach($users as $u) : ?>
						<tr>
							<td><?= esc($u->id) ?></td>
							<td><?= esc($u->username) ?></td>
							<td><?= esc($u->email) ?></td>
							<td><?= esc($u->active) ?></td>
							<td class="p-1">
								<a class="btn btn-info btn-block" href="<?= route_to('adm-users-show', $u->id) ?>">
									Detail
								</a>
							</td>
						</tr>
					<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

<?= $this->endSection() ?>
