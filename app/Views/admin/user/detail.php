<?= $this->extend("admin/templates/base") ?>


<?= $this->section('title') ?>
	Edit Post
<?= $this->endSection() ?>


<?= $this->section('content') ?>

	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="/admin">Dashboard</a>
		</li>
		<li class="breadcrumb-item active">Post</li>
	</ol>

	<div class="card mb-3">
		<div class="card-header">
			<i class="fas fa-chart-area"></i>
			Edit User</div>
		<div class="card-body">
			<?= $this->include('admin/templates/message_block') ?>

			<form action="<?= route_to('adm-users-show', $user->id) ?>" method="post">
				<?= csrf_field() ?>
				<div class="form-group">
					<label for="username">Username</label>
					<input type="username" name="username" class="form-control" id="username" value="<?= esc($user->username, 'attr') ?>">
				</div>
				<div class="form-group">
					<label for="email">Email</label>
					<input type="email" name="email" class="form-control" id="email" value="<?= esc($user->email, 'attr') ?>">
				</div>
				<div class="form-group">
					<label for="active">Active</label>
					<select name="active" class="form-control" id="active">
						<option value="1" <?= $user->active == '1' ? 'selected':'' ?>>Active</option>
						<option value="0" <?= $user->active == '0' ? 'selected':'' ?>>Passive</option>
					</select>
				</div>
				<button type="submit" class="btn btn-primary btn-block">Save User</button>
			</form>
			<hr>
			<form action="<?= route_to('adm-users-delete', $user->id) ?>" method="post">
				<?= csrf_field() ?>
				<button type="submit" class="btn btn-danger btn-block">Delete User</button>
			</form>
		</div>
	</div>

	<div class="card mb-3">
		<div class="card-header">
			<i class="fas fa-chart-area"></i>
			Change Password</div>
		<div class="card-body">
			<?= $this->include('admin/templates/message_block') ?>
			<form action="<?= route_to('adm-users-chpass', $user->id) ?>" method="post">
				<?= csrf_field() ?>
				<input type="hidden" name="email" class="form-control" id="email" value="<?= esc($user->email, 'attr') ?>">
				<div class="form-group">
					<label for="password">Password</label>
					<input type="password" name="password" class="form-control" id="password">
				</div>
				<div class="form-group">
					<label for="pass_confirm">Repeat Password</label>
					<input type="password" name="pass_confirm" class="form-control" id="pass_confirm">
				</div>
				<button type="submit" class="btn btn-primary btn-block">Update Password</button>
			</form>
		</div>
	</div>

<?= $this->endSection() ?>
