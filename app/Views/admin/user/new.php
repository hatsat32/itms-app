<?= $this->extend("admin/templates/base") ?>


<?= $this->section('title') ?>
	New Cobit
<?= $this->endSection() ?>


<?= $this->section('content') ?>

	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="/admin">Dashboard</a>
		</li>
		<li class="breadcrumb-item active">Dashboard</li>
	</ol>

	<div class="card mb-3">
		<div class="card-header">
			<i class="fas fa-chart-area"></i>
			Add User</div>
		<div class="card-body">
			<?= $this->include('admin/templates/message_block') ?>

			<form action="<?= route_to('adm-users') ?>" method="post">
				<?= csrf_field() ?>
				<div class="form-group">
					<label for="username">Username</label>
					<input type="username" name="username" class="form-control" id="username" value="<?= old('username') ?>">
				</div>
				<div class="form-group">
					<label for="email">Email</label>
					<input type="email" name="email" class="form-control" id="email" value="<?= old('email') ?>">
				</div>
				<div class="form-group">
					<label for="password">Password</label>
					<input type="password" name="password" class="form-control" id="password">
				</div>
				<div class="form-group">
					<label for="active">Active</label>
					<select name="active" class="form-control" id="active">
						<option value="1">Active</option>
						<option value="0">Passive</option>
					</select>
				</div>
				<button type="submit" class="btn btn-primary btn-block">Save User</button>
			</form>
		</div>
	</div>

<?= $this->endSection() ?>
