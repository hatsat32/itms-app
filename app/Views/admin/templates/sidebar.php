<ul class="sidebar navbar-nav">
	<li class="nav-item active">
		<a class="nav-link" href="/admin">
			<i class="fas fa-chart-line"></i>
			<span>Dashboard</span>
		</a>
	</li>
	<li class="nav-item active">
		<a class="nav-link" href="/admin/posts">
			<i class="fas fa-bookmark"></i>
			<span>Posts</span>
		</a>
	</li>
	<li class="nav-item active">
		<a class="nav-link" href="/admin/modules">
			<i class="fas fa-bookmark"></i>
			<span>Modules</span>
		</a>
	</li>
	<li class="nav-item active">
		<a class="nav-link" href="/admin/submodules">
			<i class="fas fa-bookmark"></i>
			<span>Sub Modules</span>
		</a>
	</li>
	<li class="nav-item active">
		<a class="nav-link" href="/admin/users">
			<i class="fas fa-bookmark"></i>
			<span>Users</span>
		</a>
	</li>
	<li class="nav-item active">
		<a class="nav-link" href="/admin/messages">
			<i class="fas fa-bookmark"></i>
			<span>Messages</span>
		</a>
	</li>
</ul>
