<?= $this->extend("templates/base") ?>

<?= $this->section('content') ?>

	<?php foreach($posts as $post) : ?>

		<div class="card my-4">
			<div class="card-body">
				<h2 class="card-title"><?= esc($post->name) ?></h2>
				<hr>
				<?= summary(markdown($post->content)) ?>
				<a href="/relations/<?= esc($post->id) ?>"
						class="btn btn-primary">Read More &rarr;</a>
			</div>
			<div class="card-footer text-muted">
				<?= (new \CodeIgniter\I18n\Time($post->created_at))->humanize() ?>
			</div>
		</div>

	<?php endforeach ?>

<?= $this->endSection() ?>