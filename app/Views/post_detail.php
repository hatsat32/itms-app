<?= $this->extend("templates/base") ?>

<?= $this->section('content') ?>

		<div class="card my-4">
			<div class="card-body">
				<h1 class="card-title"><?= esc($post->name) ?></h1>
				<hr>
				<?= markdown($post->content) ?>
			</div>
			<div class="card-footer text-muted">
				<?= 'Posted at: ' . (new \CodeIgniter\I18n\Time($post->created_at))->humanize() ?>
			</div>
		</div>

<?= $this->endSection() ?>