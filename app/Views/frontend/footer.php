<!-- Footer-->
<footer class="footer text-center">
	<div class="container">
		<div class="row">
			<!-- Footer Location-->
			<div class="col-lg-4 mb-5 mb-lg-0">
				<h4 class="text-uppercase mb-4">Location</h4>
				<p class="lead mb-0">Istanbul University<br/>Cerrahpaşa</p>
			</div>
			<!-- Footer Social Icons-->
			<div class="col-lg-4 mb-5 mb-lg-0">
				<h4 class="text-uppercase mb-4">Fork me on gitlab</h4>
				<a class="btn btn-outline-light btn-social mx-1" href="https://gitlab.com/hatsat32/itms-app">
					<i class="fab fa-fw fa-gitlab"></i>
				</a>
			</div>
			<!-- Footer About Text-->
			<div class="col-lg-4">
				<h4 class="text-uppercase mb-4">About ITMS</h4>
				<p class="lead mb-0">ITMS is a way to manage IT.</p>
			</div>
		</div>
	</div>
</footer>

<!-- Copyright Section-->
<div class="copyright py-4 text-center text-white">
	<div class="container"><small>Copyright © MCLee 2020</small></div>
</div>
