<?= $this->extend("frontend/base") ?>

<?= $this->section('title') ?>
	ITIL
<?= $this->endSection() ?>

<?= $this->section('content') ?>

	<section class="page-section portfolio" id="">
		<div class="container-fluid row">
			<div class="col-md-4">
				<div class="card my-4">
					<h5 class="card-header">ITIL MODULES</h5>
					<div class="card-body">
						<ul>
						<?php foreach ($modules as $module) : ?>
							<li>
								<a class="text-primary"><?= $module->name ?></a>
								<?php foreach ($module->submodules as $sub) : ?>
									<ul>
										<li><a href="/itil/<?= esc($sub->id) ?>" class="text-info"><?= $sub->name ?></a></li>
									</ul>
								<?php endforeach ?>
							</li>
						<?php endforeach ?>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="my-4">
					<?php if ($submodule_detail != null) : ?>
						<h1 class="text-center display-4"><?= $submodule_detail->name ?></h1>

						<h2>Purpose</h2>
						<?= $submodule_detail->purpose ?>

						<hr>

						<h2>Description</h2>
						<?= $submodule_detail->description ?>

						<hr>

						<h2>SVC Activity's Contribution</h2>
						<?= $submodule_detail->svc_activity_contr ?>
					<?php endif ?>
				</div>
			</div>
		</div>
	</section>

<?= $this->endSection() ?>
