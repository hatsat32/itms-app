<?= $this->extend("frontend/base") ?>

<?= $this->section('title') ?>
	About
<?= $this->endSection() ?>

<?= $this->section('content') ?>

	<!-- About Section-->
	<section class="page-section bg-primary text-white mb-0" id="about">
		<div class="container">
		</div>
	</section>

	<!-- Contact Section-->
	<section class="page-section" id="contact">
		<div class="container">
			<!-- Contact Section Heading-->
			<h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Contact Us</h2>
			<!-- Icon Divider-->
			<div class="divider-custom">
				<div class="divider-custom-line"></div>
				<div class="divider-custom-icon"><i class="fas fa-star"></i></div>
				<div class="divider-custom-line"></div>
			</div>
			<!-- Contact Section Form-->
			<div class="row">
				<div class="col-lg-8 mx-auto">

					<?php if (session()->has('message')) : ?>
						<div class="alert alert-primary" role="alert">
							<?= esc( session('message') ) ?>
						</div>
					<?php endif ?>

					<?php if (session()->has('errors')) : ?>
						<?php foreach (session('errors') as $e) : ?>
							<div class="alert alert-danger" role="alert">
								<?= esc( $e ) ?>
							</div>
						<?php endforeach ?>
					<?php endif ?>

					<form action="/messages" method="POST" novalidate="novalidate">
						<div class="control-group">
							<div class="form-group floating-label-form-group controls mb-0 pb-2">
								<label>Name</label>
								<input class="form-control" value="<?= old('name') ?>" name="name" id="name" type="text" placeholder="Name" required="required" data-validation-required-message="Please enter your name." />
								<p class="help-block text-danger"></p>
							</div>
						</div>
						<div class="control-group">
							<div class="form-group floating-label-form-group controls mb-0 pb-2">
								<label>Email Address</label>
								<input class="form-control" value="<?= old('email') ?>" name="email" id="email" type="email" placeholder="Email Address" required="required" data-validation-required-message="Please enter your email address." />
								<p class="help-block text-danger"></p>
							</div>
						</div>
						<div class="control-group">
							<div class="form-group floating-label-form-group controls mb-0 pb-2">
								<label>Message</label>
								<textarea class="form-control" name="message" id="message" rows="5" placeholder="Message" required="required" data-validation-required-message="Please enter a message."><?= old('message') ?></textarea>
								<p class="help-block text-danger"></p>
							</div>
						</div>
						<br />
						<div id="success"></div>
						<div class="form-group"><button class="btn btn-primary btn-block btn-xl" id="message" type="submit">Send</button></div>
					</form>
				</div>
			</div>
		</div>
	</section>

<?= $this->endSection() ?>
