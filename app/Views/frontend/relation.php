<?= $this->extend("frontend/base") ?>

<?= $this->section('title') ?>
	<?= esc($post->name) ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

	<section class="page-section bg-info mb-0" id="#relations">
		<div class="container">
			<div class="card my-4">
				<div class="card-body">
					<h1 class="card-title"><?= esc($post->name) ?></h1>
					<hr>
					<?= markdown($post->content) ?>
				</div>
				<div class="card-footer text-muted">
					<?= 'Posted at: ' . (new \CodeIgniter\I18n\Time($post->created_at))->humanize() ?>
				</div>
			</div>
		</div>
	</section>

<?= $this->endSection() ?>
