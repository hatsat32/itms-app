<?= $this->extend('frontend/base') ?>

<?= $this->section('title') ?>
	ITMS
<?= $this->endSection() ?>

<?= $this->section('content') ?>

	<!-- Portfolio Section-->
	<section class="page-section portfolio" id="portfolio">
		<div class="container">
			<!-- Portfolio Section Heading-->
			<h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">ITIL</h2>
			<p class="text-center lead">Itıl is 3 module</p>
			<!-- Icon Divider-->
			<div class="divider-custom">
				<div class="divider-custom-line"></div>
				<div class="divider-custom-icon"><i class="fas fa-star"></i></div>
				<div class="divider-custom-line"></div>
			</div>
			<!-- Portfolio Grid Items-->
			<div class="row">
				<!-- Portfolio Item 1-->
				<div class="col-md-6 col-lg-4 mb-5">
					<h4 class="text-center">General Management Practices</h4>
					<div class="portfolio-item mx-auto" data-toggle="modal" data-target="#portfolioModal1">
						<div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
							<div class="portfolio-item-caption-content text-center text-white"><i class="fas fa-plus fa-3x"></i></div>
						</div>
						<img class="img-fluid" src="assets/img/portfolio/cabin.png" alt="" />
					</div>
				</div>
				<!-- Portfolio Item 2-->
				<div class="col-md-6 col-lg-4 mb-5">
					<h4 class="text-center">Service Management Practices</h4>
					<div class="portfolio-item mx-auto" data-toggle="modal" data-target="#portfolioModal2">
						<div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
							<div class="portfolio-item-caption-content text-center text-white"><i class="fas fa-plus fa-3x"></i></div>
						</div>
						<img class="img-fluid" src="assets/img/portfolio/cake.png" alt="" />
					</div>
				</div>
				<!-- Portfolio Item 3-->
				<div class="col-md-6 col-lg-4 mb-5">
					<h4 class="text-center">Technical Management Practices</h4>
					<div class="portfolio-item mx-auto" data-toggle="modal" data-target="#portfolioModal3">
						<div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
							<div class="portfolio-item-caption-content text-center text-white"><i class="fas fa-plus fa-3x"></i></div>
						</div>
						<img class="img-fluid" src="assets/img/portfolio/circus.png" alt="" />
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes)-->
	<div class="scroll-to-top d-lg-none position-fixed">
		<a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
			<i class="fa fa-chevron-up"></i>
		</a>
	</div>

	<!-- itil Modals-->
	<!-- itil Modal 1-->
	<div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-labelledby="portfolioModal1Label" aria-hidden="true">
		<div class="modal-dialog modal-xl" role="document">
			<div class="modal-content">
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="fas fa-times"></i></span>
				</button>
				<div class="modal-body text-center">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-lg-8">
								<!-- Portfolio Modal - Title-->
								<h2 class="portfolio-modal-title text-secondary text-uppercase mb-0" id="portfolioModal1Label">General Management Practices</h2>
								<!-- Icon Divider-->
								<div class="divider-custom">
									<div class="divider-custom-line"></div>
									<div class="divider-custom-icon"><i class="fas fa-star"></i></div>
									<div class="divider-custom-line"></div>
								</div>
								<!-- Portfolio Modal - Image-->
									<!-- <img class="img-fluid rounded mb-5" src="assets/img/portfolio/cabin.png" alt="" /> -->
								<!-- Portfolio Modal - Text-->
								<?php foreach ($generals as $g) : ?>
									<a class="btn btn-primary my-2" href="<?= route_to('itil-detail', $g->id) ?>"><?= esc($g->name) ?></a>
								<?php endforeach ?>
								<!-- <button class="btn btn-primary" data-dismiss="modal"><i class="fas fa-times fa-fw"></i>Close Window</button> -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- itil Modal 2-->
	<div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-labelledby="portfolioModal2Label" aria-hidden="true">
		<div class="modal-dialog modal-xl" role="document">
			<div class="modal-content">
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="fas fa-times"></i></span>
				</button>
				<div class="modal-body text-center">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-lg-8">
								<!-- Portfolio Modal - Title-->
								<h2 class="portfolio-modal-title text-secondary text-uppercase mb-0" id="portfolioModal2Label">Service Management Practices</h2>
								<!-- Icon Divider-->
								<div class="divider-custom">
									<div class="divider-custom-line"></div>
									<div class="divider-custom-icon"><i class="fas fa-star"></i></div>
									<div class="divider-custom-line"></div>
								</div>
								<!-- Portfolio Modal - Image-->
								<!-- <img class="img-fluid rounded mb-5" src="assets/img/portfolio/cake.png" alt="" /> -->
								<!-- Portfolio Modal - Text-->
								
								<?php foreach ($services as $s) : ?>
									<a class="btn btn-primary my-2" href="<?= route_to('itil-detail', $s->id) ?>"><?= esc($s->name) ?></a>
								<?php endforeach ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- itil Modal 3-->
	<div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-labelledby="portfolioModal3Label" aria-hidden="true">
		<div class="modal-dialog modal-xl" role="document">
			<div class="modal-content">
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="fas fa-times"></i></span>
				</button>
				<div class="modal-body text-center">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-lg-8">
								<!-- Portfolio Modal - Title-->
								<h2 class="portfolio-modal-title text-secondary text-uppercase mb-0" id="portfolioModal3Label">Technical Management Practices</h2>
								<!-- Icon Divider-->
								<div class="divider-custom">
									<div class="divider-custom-line"></div>
									<div class="divider-custom-icon"><i class="fas fa-star"></i></div>
									<div class="divider-custom-line"></div>
								</div>
								<!-- Portfolio Modal - Image-->
								<!-- <img class="img-fluid rounded mb-5" src="assets/img/portfolio/circus.png" alt="" /> -->
								<!-- Portfolio Modal - Text-->
								
								<?php foreach ($technicals as $t) : ?>
									<a class="btn btn-primary my-2" href="<?= route_to('itil-detail', $t->id) ?>"><?= esc($t->name) ?></a>
								<?php endforeach ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?= $this->endSection() ?>
