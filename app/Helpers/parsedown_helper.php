<?php

use Config\Services;


if (! function_exists('markdown'))
{
	/**
	 * @param $text - markdown string
	 * @return string - html text
	 */
	function markdown($text)
	{
		$parsedown = Services::parsedown();
		return $parsedown->text($text);
	}
}

if (! function_exists('summary'))
{
	/**
	 * @param $text - markdown string
	 * @return string - html text
	 */
	function summary($text)
	{
		$dom = Services::dom();

		$content = mb_convert_encoding($text, 'HTML-ENTITIES', 'UTF-8');
		$dom->loadHTML($content);
		
		$ptags = $dom->getElementsByTagName("p");
		if (! empty($ptags))
		{
			return $dom->saveHTML($ptags[0]);
		}

		return '';
	}
}
