<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class InitialTables extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id'          => ['type' => 'int', 'auto_increment' => true],
			'name'        => ['type' => 'varchar', 'constraint' => '250', 'unique' => true],
			'description' => ['type' => 'text', 'null' => true],
			'created_at'  => ['type' => 'datetime', 'null' => true],
			'updated_at'  => ['type' => 'datetime', 'null' => true],
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('modules');

		$this->forge->addField([
			'id'          => ['type' => 'int', 'auto_increment' => true],
			'module_id'   => ['type' => 'int'],
			'name'        => ['type' => 'varchar', 'constraint' => '255', 'unique' => true],
			'purpose'     => ['type' => 'text', 'null' => true],
			'description' => ['type' => 'text', 'null' => true],
			'svc_activity_contr' => ['type' => 'text', 'null' => true],
			'image'       => ['type' => 'text', 'null' => true],
			'created_at'  => ['type' => 'datetime', 'null' => true],
			'updated_at'  => ['type' => 'datetime', 'null' => true],
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('sub_modules');

		$this->forge->addField([
			'id'          => ['type' => 'INT', 'auto_increment' => true],
			'type'        => ['type' => 'varchar', 'constraint' => '100'],
			'name'        => ['type' => 'varchar', 'constraint' => '250', 'unique' => true],
			'content'     => ['type' => 'text', 'null' => true],
			'created_at'  => ['type' => 'datetime', 'null' => true],
			'updated_at'  => ['type' => 'datetime', 'null' => true],
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('posts');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('users', true);
		$this->forge->dropTable('modules', true);
		$this->forge->dropTable('sub_modules', true);
		$this->forge->dropTable('posts', true);
	}
}
