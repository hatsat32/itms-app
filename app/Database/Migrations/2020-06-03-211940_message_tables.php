<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MessageTables extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id'          => ['type' => 'INT', 'auto_increment' => true],
			'name'        => ['type' => 'varchar', 'constraint' => '100'],
			'email'       => ['type' => 'varchar', 'constraint' => '100'],
			'message'     => ['type' => 'text'],
			'created_at'  => ['type' => 'datetime', 'null' => true],
			'updated_at'  => ['type' => 'datetime', 'null' => true],
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('messages');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('messages', true);
	}
}
