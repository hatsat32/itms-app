<?php namespace App\Models;

use CodeIgniter\Model;
use CodeIgniter\Entity;

class User extends Model
{
	protected $table      = 'users';
	protected $primaryKey = 'id';
	protected $returnType = Entity::class;

	protected $allowedFields = [
		'username', 'email', 'active', 'password_hash'
	];

	protected $validationRules = [
		'username'      => 'required|min_length[3]|max_length[30]|alpha_numeric|is_unique[users.username,username,{username}]',
		'email'         => 'required|max_length[255]|valid_email|is_unique[users.email,email,{email}]',
		'password_hash' => 'required',
		'active'        => 'required',
	];

	protected $useTimestamps = true;
}
