<?php namespace App\Models;

use CodeIgniter\Model;
use CodeIgniter\Entity;

class Module extends Model
{
	protected $table      = 'modules';
	protected $primaryKey = 'id';
	protected $returnType = Entity::class;

	protected $allowedFields = [
		'name', 'description'
	];

	protected $validationRules = [
		'name'        => 'required|string|max_length[255]',
		'description' => 'required|string',
	];

	protected $useTimestamps = true;
}
