<?php namespace App\Models;

use CodeIgniter\Model;
use CodeIgniter\Entity;

class Message extends Model
{
	protected $table      = 'messages';
	protected $primaryKey = 'id';
	protected $returnType = Entity::class;

	protected $allowedFields = [
		'name', 'email', 'message'
	];

	protected $validationRules = [
		'name'    => 'required|string|max_length[100]',
		'email'   => 'required|valid_email|max_length[100]',
		'message' => 'required|string',
	];

	protected $useTimestamps = true;
}
