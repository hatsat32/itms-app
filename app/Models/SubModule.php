<?php namespace App\Models;

use CodeIgniter\Model;
use CodeIgniter\Entity;

class SubModule extends Model
{
	protected $table      = 'sub_modules';
	protected $primaryKey = 'id';
	protected $returnType = Entity::class;

	protected $allowedFields = [
		'module_id', 'name', 'purpose', 'description', 'svc_activity_contr'
	];

	protected $validationRules = [
		// 'module_id'          => 'required ',
		'name'               => 'required|string|max_length[255]',
		'purpose'            => 'required|string',
		'description'        => 'required|string',
		'svc_activity_contr' => 'required|string',
	];

	protected $useTimestamps = true;
}
