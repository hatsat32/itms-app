<?php namespace App\Models;

use CodeIgniter\Model;
use CodeIgniter\Entity;

class Post extends Model
{
	protected $table      = 'posts';
	protected $primaryKey = 'id';
	protected $returnType = Entity::class;

	protected $allowedFields = [
		'type', 'name', 'content',
	];

	protected $validationRules = [
		'type'    => 'required|in_list[itil,cobit,iso27001,iso9001]',
		'name'    => 'required|string|min_length[3]|max_length[100]',
		'content' => 'required|string',
	];

	protected $useTimestamps = true;
}
