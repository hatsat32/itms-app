<?php namespace Config;

use CodeIgniter\Router\RouteCollection;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(false);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');

$routes->get('/cobit',       'Home::cobit');
$routes->get('/itil',        'Home::itil');
$routes->get('/itil/(:num)', 'Home::itil/$1', ['as' => 'itil-detail']);
$routes->get('/iso27001',    'Home::iso27001');
$routes->get('/iso9001',     'Home::iso9001');

$routes->get('relations',        'Home::relations');
$routes->get('relations/(:num)', 'Home::relation/$1');

$routes->get('/about',     'Home::about');
$routes->post('/messages', 'Home::message');

$routes->group('', ['namespace' => 'App\Controllers'], function($routes) {
    // Login/out
    $routes->get('login', 'AuthController::login', ['as' => 'login']);
    $routes->post('login', 'AuthController::attemptLogin');
    $routes->get('logout', 'AuthController::logout');

    // Registration
    $routes->get('register', 'AuthController::register', ['as' => 'register']);
    $routes->post('register', 'AuthController::attemptRegister');

    // Activation
    $routes->get('activate-account', 'AuthController::activateAccount', ['as' => 'activate-account']);

    // Forgot/Resets
    $routes->get('forgot', 'AuthController::forgotPassword', ['as' => 'forgot']);
    $routes->post('forgot', 'AuthController::attemptForgot');
    $routes->get('reset-password', 'AuthController::resetPassword', ['as' => 'reset-password']);
    $routes->post('reset-password', 'AuthController::attemptReset');
});

$routes->group('admin', ['namespace' => 'App\Controllers\Admin'], function(RouteCollection $routes) {
	$routes->get('/', 'Dashboard::index', ['as' => 'adm']);

	$routes->group('users', function(RouteCollection $routes) {
		$routes->get('',               'User::index',   ['as' => 'adm-users']);
		$routes->get('new',            'User::new',     ['as' => 'adm-users-new']);
		$routes->get('(:num)',         'User::show/$1', ['as' => 'adm-users-show']);
		$routes->post('',              'User::create');
		$routes->post('(:num)/delete', 'User::delete/$1', ['as' => 'adm-users-delete']);
		$routes->post('(:num)',        'User::update/$1');
		$routes->post('(:num)/chpass', 'User::change_password/$1', ['as' => 'adm-users-chpass']);
	});

	$routes->group('posts', function(RouteCollection $routes) {
		$routes->get('/',              'Post::index',   ['as' => 'adm-posts']);
		$routes->get('new',            'Post::new',     ['as' => 'adm-posts-new']);
		$routes->get('(:num)',         'Post::show/$1', ['as' => 'adm-posts-show']);
		$routes->post('/',             'Post::create');
		$routes->post('(:num)/delete', 'Post::delete/$1', ['as' => 'adm-posts-delete']);
		$routes->post('(:num)',        'Post::update/$1');
	});

	$routes->group('modules', function(RouteCollection $routes) {
		$routes->get('/',              'Module::index',   ['as' => 'adm-modules']);
		$routes->get('new',            'Module::new',     ['as' => 'adm-modules-new']);
		$routes->get('(:num)',         'Module::show/$1', ['as' => 'adm-modules-show']);
		$routes->post('/',             'Module::create');
		$routes->post('(:num)/delete', 'Module::delete/$1', ['as' => 'adm-modules-delete']);
		$routes->post('(:num)',        'Module::update/$1');
	});

	$routes->group('submodules', function(RouteCollection $routes) {
		$routes->get('/',              'SubModule::index',   ['as' => 'adm-submodules']);
		$routes->get('new',            'SubModule::new',     ['as' => 'adm-submodules-new']);
		$routes->get('(:num)',         'SubModule::show/$1', ['as' => 'adm-submodules-show']);
		$routes->post('/',             'SubModule::create');
		$routes->post('(:num)/delete', 'SubModule::delete/$1', ['as' => 'adm-submodules-delete']);
		$routes->post('(:num)',        'SubModule::update/$1');
	});

	$routes->group('messages', function(RouteCollection $routes) {
		$routes->get('/',              'Message::index',   ['as' => 'adm-messages']);
		$routes->get('(:num)',         'Message::show/$1', ['as' => 'adm-messages-show']);
		$routes->post('(:num)/delete', 'Message::delete/$1', ['as' => 'adm-messages-delete']);
		$routes->post('(:num)',        'Message::update/$1');
	});
});

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need to it be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
